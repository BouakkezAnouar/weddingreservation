<?php
session_start();

if(!isset($_SESSION['isLogged'])){ //if login in session is not set
    header("Location: login.php");
    exit(0);
}

if($_SESSION['admin']!= 1){ //if login in session is not set
    header("Location: index.php");
    exit(0);
}

$nom ='';
$error ='' ;
$prix ='';
$owner ='';
$ownerPhone= '';
$description ='';

if ( (isset($_POST['nom'])) && (isset($_POST['prix'])) && (isset($_POST['owner'])) && (isset($_POST['ownerPhone'])) && (isset($_POST['description'])) ){

   if ( (!empty(trim($_POST['nom']))) && (!empty(trim($_POST['prix']))) && (!empty(trim($_POST['owner']))) && (!empty(trim($_POST['ownerPhone']))) && (!empty(trim($_POST['description']))) ){

try
{
    include("connection.php");
    //test if nom exist 
    $stmt = $conn->prepare("SELECT * FROM salle WHERE nom = ?");
    $stmt->execute([$_POST['nom']]);
    $salle = $stmt->fetch();
    if ($salle){
        $error ="Choisir un autre nom svp !";
        //vider le champs nom
        $nom="";
    }
    else 
    {
        $nom= $_POST['nom'];
        $prix =$_POST['prix'];
        $owner=$_POST['owner'];
        $description=$_POST['description'];
        $ownerPhone=$_POST['ownerPhone'];
        $sql = "INSERT INTO salle VALUES ('', '$nom' , '$prix' , '$description', '$owner', '$ownerPhone' )";
        // use exec() because no results are returned
        $conn->exec($sql);
        echo "insertion de salle $nom avec succès";
        
        //vider tous les champs
        $nom= '';
        $prix ='';
        $owner='';
        $description='';
        $ownerPhone='';
    }
    //close connection
    $conn = null;
}
catch(PDOException $e)
{
echo $sql . "<br>" . $e->getMessage();
    //close connection
    $conn = null;
}
}
else {
    $error =  "all champs must be remplied";
}
}

?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8"/>
        <title>Create Account</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
  <script>
        function vérifier() {
          var x= document.forms["Form"]["First"].value;
          var y= document.forms["Form"]["Last"].value;
          var z=document.forms["Form"]["User"].value;
          var t=document.forms["Form"]["Password"].value;
          var r=document.forms["Form"]["Confirm"].value;
          var s=document.forms["Form"]["Email"].value;
          if (x == "" || y=="" || z=="" || t=="" || r=="" || s =="") {
            alert("Fill in your form ");
            return false;
          }
        }
        </script>
        <style>
            @font-face {
    font-family: 'houssem';
    src: url('cavier_dream/CaviarDreams.ttf');}
    body{
        background-image: url("images/41FAM8Tx18L._SX466_.jpg");
        background-size: 100%;
        height: 700px;
        
    }
    h1
    {
        font-family: "houssem";
        font-weight:100;
        
        
    }
    form{
        display: flex;
        flex-direction: column;

    } 
    .input
    {   padding: 15px;
        margin: 10px;
        border-radius:5px;
        border: solid rgb(199, 196, 196) 0.5px;
        font-family: "houssem";
        width: 450px;
        height: 45px;

    }
    .submit
    {
        padding: 10px;
        margin: 10px;
        border-radius:5px;
        border: solid rgb(74, 180, 74) 0.5px;
        font-family: "houssem";
        font-weight:500;
        font-size: 17px;
        background-color:rgb(74, 180, 74);
        width:450px;
        height: 45px;
        color: azure;


        
    }div
    {
        background-color:#f1f1f1;
        width: 500px;
        height:550px ;
        margin-top:100px;
        margin-left:auto;
        margin-right: auto;
        padding: 14px 16px;
        box-shadow: #313030 5px 5px 30px ;
        border: none;
        border-radius: 5px;
    }.submit:hover
    {
        opacity: 0.8;
        font-weight:bolder;
    } 
    

        </style>
    </head>
    <body>
        <div>
                <h1 class="text-center text-muted">Insertion salle</h1>
                <form method="POST" action="insertionSalle.php" name="Form">
                    <input class="input" type="text" placeholder="Salle Name" name="nom" required/>
                    <input class="input" type="text" placeholder="Prix" name="prix" required/>
                    <input class="input" type="text" placeholder="Owner" name="owner" required/>
                    <input class="input" type="text" placeholder="Owner Phone" name="ownerPhone" required/>
                    Description 
                    <textarea rows="4" cols="50" name="description"></textarea>
                    <?php
                if ($error !='')
                 echo '<span class="alert alert-danger text-center">' . $error . '</span>' ;
                ?>
                    <input class="submit"type="submit" value="Insert">
                   
                </form>
        </div>
    </body>    
</html>
