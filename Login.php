
<?php
// Start the session
session_start();

 if(isset($_SESSION['isLogged'])){ //if login in session is not set
    header("Location: index.php");
}
$email ="";
$password ="" ;
$error="";
if (isset($_POST['email']) && isset($_POST['password']))
{
    include("connection.php");
    $email = htmlspecialchars($_POST['email']);
    $password =htmlspecialchars($_POST['password']);
    
    //if email or password is vide
    if(empty(trim($email)) || empty(trim($password)))
        $error = "all champs must be remplied !!" ;
    else {
    $stmt = $conn->prepare("SELECT * FROM users WHERE email = ? and password = ?");
    $stmt->execute([$email, $password]);
    $user = $stmt->fetch();
    if ($user)
    {
        //create session 
        session_start();
        $_SESSION['isLogged']="naw";
        $_SESSION['id']=$user['id'];
        $_SESSION['nom']=$user['nom'];
        $_SESSION['email']=$user['email'];
        $_SESSION['admin']=$user['admin'];
        header("Location: index.php");
    } else {
        $error = "email or password is worng !";
    }
}
//close connection
$conn = null;
    
}
?>



<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8"/>
        <title>Log In</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/animejs/2.0.2/anime.min.js"></script>
  <link rel="shortcut icon" href="images/Log-in.png" />

  <script>
     
        </script>
        <style>
            @font-face {
    font-family: 'houssem';
    src: url('cavier_dream/CaviarDreams.ttf');}
    body{
        background-color:rgba(60, 147, 228, 0.938);
        background-size: 100%;
        height: 650px;
        
    }
    h1
    {
        font-family: "houssem";
        font-weight:100;
        
        
    }
    form{
        display: flex;
        flex-direction: column;

    } 
    #titre
    {
        font-size: 80px;
        margin-top:30px;
    }
    .input
    {   padding: 15px;
        margin: 10px;
        border-radius:5px;
        border: solid rgb(199, 196, 196) 0.5px;
        font-family: "houssem";
        width: 450px;
        height: 45px;

    }
    .submit
    {
        padding: 10px;
        margin: 10px;
        border-radius:5px;
        border: solid rgb(199, 196, 196) 0.5px;
        font-family: "houssem";
        font-weight:500;
        font-size: 17px;
        background-color:rgb(29, 149, 230);
        width:450px;
        height: 45px;
        color: azure;


        
    }.form
    {
        background-color:#f1f1f1;
        width: 500px;
        height:450px ;
        margin-top:30px;
        margin-left:auto;
        margin-right: auto;
        padding: 14px 16px;
        box-shadow: #313030 5px 5px 30px ;
        border: none;
        border-radius: 5px;
    }.submit:hover
    {
        opacity: 0.8;
        font-weight:bolder;
    } #A1
    {   
        text-decoration: none;
        font-family: "houssem";
        margin-top:20px;
        font-size:20px;
        
    }#A2
    {
        text-decoration: none;
        font-family: "houssem";
        
        
    
    }p
    {
      margin-top:20px;  
    }
    #A2:hover{
        font-size:30px;
        opacity:0.6;
    }#A1:hover
    {
        font-size:21px;
    }    

        </style>
    </head>
    <body>
            <h1 id="titre"class="text-center text-white">Welcom back !</h1>
        <div class="form">
                <img src="images/1000.svg" alt="log in icon" width="80" height="80" class="mx-auto d-block"/>
    
                  <h1 class="text-center text-muted">Log In</h1>
                <form method="POST" action="Login.php">
                
                    <input class="input" type="email" placeholder="Email" name="email" required/>
                    <input class="input" type="password" placeholder="Password" name="password"required/>
                    <?php
                if ($error !='')
                 echo '<div class="alert alert-danger text-center">' . $error . '</div>' ;
                ?>
                    <input class="submit"type="submit" value="Log In">
                     <a href="" id="A1" class="text-muted text-center">Forget your password ?</a>

                </form>
                
                </div>
                <p class=" text-center"><a href="formulaire.html" id="A2" class="text-white">Don't have an account?Get started</a></p>

               
    </body>    
</html>
