<?php
// Start the session
session_start();


if(!isset($_SESSION['isLogged'])){ //if login in session is not set
  header("Location: login.php");
  exit(0);
}


?>
<!DOCTYPE html>
<html>

<head>
<meta charset="utf-8" />    
        
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="bootstrap-4.0.0/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="bootstrap-4.0.0/assets/js/vendor/popper.min.js"></script>
        <script src="bootstrap-4.0.0/js/bootstrap.min.js"></script>
        <script src="bootstrap-4.0.0/js/src/dropdown.js"></script>
        <script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />

<script>
   let currentTime =  new Date($.now()) ; 

  var startD = currentTime.getTime() / 1000  ; 
  var endD = currentTime.getTime() /  1000  ; 
  var idUser  ;
  var idSalle ;


$(function() {
  $('input[name="daterange"]').daterangepicker({
    opens: 'left'
  }, function(start, end, label) {
    // console.log("A new date selection was made: " +start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
    start = new Date (start) ; 
    end = new Date(end);
    startD = `${start.getFullYear()}-${start.getMonth()+1}-${start.getDate()}`;
    endD = `${end.getFullYear()}-${end.getMonth()+1}-${end.getDate()}`;

    console.log(startD);
    console.log(endD);
   
  });
   
  $('input[name="daterange"]').data('daterangepicker').setStartDate(currentTime);
  $('input[name="daterange"]').data('daterangepicker').setEndDate( currentTime) ;



});

function book(idUser, idSalle){
    window.location.href = `booki.php?idUser=${idUser}&idSalle=${idSalle}&start=${startD}&end=${endD}` ;
}

</script>
        <link rel="shortcut icon" href="images/R.png" />

        <style>
          @font-face {
    font-family: 'houssem';
    src: url('cavier_dream/CaviarDreams.ttf');
   
    ;}
        body{
          margin: 50px;
        }
        .Div{
          width: 80%;
          height: 250px;
          border:solid 0.5px gainsboro;
          border-radius: 5px;
          margin-top:20px;
          margin-bottom: 20px;
          margin-left:10%;
          padding:10px;
          box-shadow: rgb(202, 200, 200) 2px 2px 5px 0.1px;
        }.Div:hover
        {
          box-shadow: rgb(168, 167, 167) 2px 2px 10px 0.1px;
          ;}
          #image-1:hover
          {
            opacity: 0.9;}
            #image-2:hover
          {
            opacity: 0.9;
          }#image-4:hover
          {
            opacity: 0.9;}
            #image-6:hover
          {
            opacity: 0.9;}
            #image-7:hover
          {
            opacity: 0.9;
          }#Div1
          {
            margin-top: 50px;
          }#Home
          { 
            display : inline-block ;
            padding: 14px;
            border:solid 0.5px gainsboro;
             border-radius: 5px;
             margin-right :100px ;
            
            font-family: 'houssem';
            font-size: 20px;
            text-decoration: none;
            box-shadow: rgb(241, 237, 237) 2px 2px 1px 0.1px;
            margin-left: 10%;

          }#Home:hover
            {
          box-shadow: rgb(168, 167, 167) 2px 10px 10px 0.1px;}
            .col-8 h5{
               color: orange;
               font-family: 'houssem';
               font-weight: bold;

            }.book
            {
              float: right;
              margin-top:150px;
              width:110px;
              height:45px ;
              font-family: 'Philosopher';
              
              border: none;
              border-radius: 5px;
              font-size: 20px;
              color: #f1f1f1;
            }.book:hover
            {
              width: 120px;
            }.coordonnées
            {
              margin-top: 50px;
              margin-left:65px;
            }span
            {
              font-family: "houssem";
              font-size: 20px;
            }.pagination
            {
              display: block;
              
              font-size: 15px;
              margin-top:50px;
              margin-left:200px;
            }.pagination a{
              padding: 14px;
              text-decoration: none;

              
              
            }.pagination #A1.active {
             background-color:rgb(247, 74, 74);
             color: white;
             border-radius: 5px;
            }.pagination a:hover:not(.active)
            {
              background-color:rgb(221, 217, 217);
              border-radius: 5px;
            }
          
        </style>
</head>
<body>
  <a href="index.php" id="Home"> Home </a>
  <?php
  if (isset($_GET['error']) && !empty($_GET['error'])) {
  if (strrpos($_GET['error'], '--') ==true )
  echo '<span class="alert alert-success">' . $_GET['error'] . '</span>' ;

  else 
    echo '<span class="alert alert-danger">' . $_GET['error'] . '</span>' ;
  }
  ?>
  

<?php

try
{
    include("connection.php");
    //test if nom exist 
    $stmt = $conn->query("SELECT * FROM salle");
    $stmt->execute([]);
    while ($salle = $stmt->fetch()) {
     echo '<div class="Div row" id="Div">
     <div class="col-4">
     <img src="images/1.jpg" class="img-thumbnail"  width="300" id="image-1" data-toggle="modal" data-target="#information"> 
     <div class="modal fade" id="information">
         <div class="modal-dialog modal-lg">
           <div class="modal-content">
           
             <!-- Modal Header -->
             <div class="modal-header">
               <h4 class="modal-title">'. $salle['nom'] . '</h4>
               <button type="button" class="close" data-dismiss="modal">×</button>
             </div>
             
             <!-- Modal body -->
             <div class="modal-body">
               <img src="images/1.jpg" width="80%" class="mx-auto d-block img-thumbnail">
               <div class="coordonnées">
                   <p><span>Prix/heure:'. $salle['prix'] .'</span></p>
                   <p><span>téléphone:' . $salle['ownerPhone'] .  '</span></p>
                   <p><span>Owner:'     . $salle['owner'] .  '</span></p>
                 </div>
             </div>
             
             
             <!-- Modal footer -->
             <div class="modal-footer">
               <button type="button" class="btn btn-danger" data-dismiss="modal">Book</button>
             </div>
             
           </div>
         </div>
       </div>
   </div>
     <div class="col-8">
         <h5 class="text-danger">'. $salle['nom']. '</h5>
         <form>
         <!-- Button trigger modal -->
         <a type="button" class="btn btn-success" href="reservations.php?id='. $salle['id'] .'" >reservations</a>
         <button type="button" class="btn btn-danger book" data-toggle="modal" data-target="#exampleModal'. $salle['id'].'">
             Book
          </button>
        
         <!-- Modal -->
         <div class="modal fade" id="exampleModal'. $salle['id'].'" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
           <div class="modal-dialog" role="document">
             <div class="modal-content">
               <div class="modal-header">
                 <h5 class="modal-title" id="exampleModalLabel">Nombre de jours :</h5>
                 <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                   <span aria-hidden="true">&times;</span>
                 </button>
               </div>
               <div class="modal-body">
               <input type="text" name="daterange" value="01/01/2018 - 01/15/2018" />
               </div>
               <div class="modal-footer">
                 <button type="button" class="btn btn-primary booki" onclick="book('.$_SESSION['id'] .',' . $salle['id']. ')">Save changes</button>
               </div>
             </div>
           </div>
         </div>
         </form>
         <p style="font-family:"Segoe UI", Tahoma, Geneva, Verdana, sans-serif; font-size: 20px;"><span style="font-weight: bold">Description: </span>
           '.$salle['description'] . '
          </p>
     </div>
     
  </div>' ;
  }


}catch(PDOException $e)
  {
  echo $sql . "<br>" . $e->getMessage();
      //close connection
      $conn = null;
  }

?>


 
 


<hr>
<div class="pagination" >
  <a href="" id="A1" class="active"><span style="font-size:20px;font-weight: bold; ">1</span></a>
  <a href=""><span style="font-size:20px; font-weight: bold;">2</span></a>
  <a href=""><span style="font-size:20px; font-weight: bold;">3</span></a>
  <a href=""><span style="font-size:20px; font-weight: bold;">4</span></a>
  <a href=""><span style="font-size:20px; font-weight: bold;">Next &raquo</span></a>
</div>
</body>
</html>