<?php
session_start();
?>
<!DOCTYPE html>
<html>

<head>
  <title>زفاف</title>
  <meta charset="utf-8" />
  <link rel="stylesheet" href="home.css" />
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="bootstrap-4.0.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="bootstrap-4.0.0/assets/js/vendor/popper.min.js"></script>
  <script src="bootstrap-4.0.0/js/bootstrap.min.js"></script>
  <script src="bootstrap-4.0.0/js/src/dropdown.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/animejs/2.0.2/anime.min.js"></script>
  <link rel="shortcut icon" href="images/R.png" />

  <style>
    body {
      background-image: url("images/audience-backlit-black-and-white-2078076.jpg");
      background-size: 100%;
      height: 2000px;
      background-repeat: no-repeat;
    }

    @font-face {
      font-family: 'houssem';
      src: url('cavier_dream/CaviarDreams.ttf');
      font-family: 'ayoub';
      src: url('brushield/Brushield.ttf')
    }

    .ml5 {
      position: relative;
      font-weight: 300;
      font-size: 4.5em;

    }

    .ml5 .text-wrapper {
      position: relative;
      display: inline-block;
      padding-top: 0.1em;
      padding-right: 0.05em;
      padding-bottom: 0.15em;
      line-height: 1em;
    }

    .ml5 .line {
      position: absolute;
      left: 0;
      top: 0;
      bottom: 0;
      margin: auto;
      height: 3px;
      width: 100%;
      background-color: orange;
      transform-origin: 0.5 0;
    }

    .ml5 .ampersand {
      font-family: 'houssem';
      font-style: italic;
      font-weight: 400;
      width: 1em;
      margin-right: 0.5em;
      margin-left: -0.1em;
    }

    .ml5 .letters {
      display: inline-block;
      opacity: 0;
    }

    .ml12 {
      font-weight: 200;
      font-size: 80px;
      text-transform: uppercase;
      letter-spacing: 0.5em;
      font-family: 'ayoub';
    }

    .ml12 .letter {
      display: inline-block;
      line-height: 1em;
    }

    .nav {
      font-family: "houssem";
      background-color: black;
    }

    .nav a {
      float: left;
      display: block;
      color: #f2f2f2;
      text-align: center;
      padding: 14px 16px;
      text-decoration: none;
      font-size: 16px;
    }

    .nav a:hover {
      color: azure;
      background-color: #661111;


    }

    .nav-link {
      margin-left: 5px;
    }

    .dropdown-menu {
      width: 400px;
      margin-top: 0px;

    }

    .dropdown-menu a {
      color: black;
      font-size: 15px;

    }

    .dropdown-menu hr {
      width: 300px;
    }

    .carousel-inner img {
      width: 100%;
      height: 550px;
    }

    h1 {
      margin-bottom: 100px;
      font-family: "houssem";

    }

    #H2 {
      font-size: 150px;
      color: orange;
      font-family: 'ayoub'
    }

    #titre {
      margin-left: 80px;
      margin-right: 300px;
      margin: 50px;
    }

    #paragraphe {
      width: 600px;
      margin-left: 50px;
    }

    textarea {
      border: solid black 0.2px;
      height: 150px;
      width: 550px;
      margin-top: 15px;
      padding: 10px;
      font-family: "houssem";
    }

    input[type="email"],
    input[type="tel"] {
      width: 273px;
      border: solid black 0.2px;
      height: 50px;
      padding: 14px;
      font-family: "houssem"

    }

    input[type="submit"] {
      background-color: #660202;
      width: 150px;
      height: 40px;
      border: none;
      border-radius: 1px;
      float: right;
      margin-right: 12px;
      font-size: 25px;
      font-family: "houssem";
      color: wheat;


    }

    input[type="submit"]:hover {
      background-color: #ac0d0d
    }

    .form {
      height: 300px;
      width: 90%;
      background-color: black;
      margin-left: 20px;
      margin: auto;
      box-shadow: 6px 6px 6px rgb(134, 132, 132);


    }

    form {
      padding-top: 20px;
      padding-left: 10px;

    }

    #P2,
    hr {
      margin-left: 30px;
    }

    hr {
      width: 500px;
    }

    #section1 {
      margin-bottom: 0px;
      width: 100%;
      height: 400px;
      padding: 50px 16px;
      background-color: rgb(22, 22, 22);
      column-count: 4;
      column-gap: 50px;
      column-rule: 0.2px solid white;
      color: rgb(190, 186, 186);
      font-family: 'houssem'
    }

    .images {
      display: flex;
      justify-content: space-around;

      ;

    }

    #img1,
    #img2,
    #img7 {
      width: 350px;
      height: 250px;

    }

    .pol {
      width: 350px;
      box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
    }

    .container {
      margin-top: 15px;

    }

    .pol:hover {
      box-shadow: 10px 10px 16px rgba(0, 0, 0, 0.2), 5px 16px 20px rgba(0, 0, 0, 0.2);
    }

    #footer {

      height: 100px;

    }

    .logo {
      margin-top: 50px;
      margin-left: 45%;
    }

    #logo1:hover,
    #logo2:hover,
    #logo3:hover {
      opacity: 1;
    }

    #logo1,
    #logo2,
    #logo3 {
      opacity: 0.9;
    }
  </style>
</head>

<body>
  <div class="nav">
    <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
      menu</a>
    <div class="dropdown-menu">
    <?php 
       if(!isset($_SESSION['isLogged']))
          echo   '<a href="Login.php" title="Log in" class="dropdown-item "> Log in</a>' ;
      if(!isset($_SESSION['isLogged']))
          echo '<a href="formulaire.php" title="sign up" class="dropdown-item"> Sign up </a>' ;
      if(isset($_SESSION['isLogged']))
          echo '<a href="page1.php" title="Our Rooms" class="dropdown-item divider">Our Rooms</a>' ;
          if(isset($_SESSION['isLogged']))
          echo '<a href="reserved.php?idUser='. $_SESSION['id'] .'" title="Our reservations" class="dropdown-item divider">Our Reservations</a>' ;
          if(isset($_SESSION['isLogged']) && $_SESSION['admin']==1)
          echo '<a href="insertionSalle.php" title="Insertion salle" class="dropdown-item divider">Insertion salle</a>' ;
    ?>

      <a href="" title="About" class="dropdown-item">About</a>
      <a href="" title="Help" class="dropdown-item">Help</a>



      <a href="" title="Offer" class="dropdown-item">Offer</a>
      <?php 
      
      if(isset($_SESSION['isLogged']))
      
      echo'<a href="Logout.php" title="Log out" class="dropdown-item divider">Log out</a>';
      ?>




    </div>

  </div>


  </div>


  </head>

  <body>

    <div id="demo" class="carousel slide" data-ride="carousel">

      <div class="carousel-inner">
        <div class="carousel-item active">
          <img src="images/10.jpg">
          <div class="carousel-caption">

            <h1 class="ml5">
              <span class="text-wrapper">
                <span class="line line1"></span>
                <span class="letters letters-left">WELCOM </span>
                <span class="letters ampersand" style="color: orange;">TO</span>
                <span class="letters letters-right"> WEBSITE-TITEL</span>
                <span class="line line2"></span>
              </span>
            </h1>









            <p>Search and book event rooms for all occasions</p>
          </div>
        </div>
        <div class="carousel-item">
          <img src="images/9.jpg">
          <div class="carousel-caption">
            <h1 class="ml12 text-warning">New Offers</h1>

            <p id="P2">Here you will find rooms for parties</p>
          </div>
        </div>
        <div class="carousel-item">
          <img src="images/1.jpg">
          <div class="carousel-caption">

            <p>Search our party venues, party room rentals, and party halls to identify the perfect ...
              we'll help you find and book the perfect party venue for your next big party<br>
              View Party Room operational hours and hourly pricing here, private spaces for events and
              parties available for groups of 6 and above. Book with us today!.</p>
          </div>
        </div>
        <div class="carousel-item">
          <img src="images/4.jpg">
          <div class="carousel-caption">
            <h1 id="H2">Hurry up!</h1>
            <p id="P2">Here you will find rooms for parties</p>
          </div>
        </div>
        <a class="carousel-control-prev" href="#demo" data-slide="prev">
        </a>
        <a class="carousel-control-next" href="#demo" data-slide="next">
        </a>
      </div>
    </div>
    <div class="row">
      <h2 id="titre">A truly inspirational venue, the historic buildings,
        fascinating galleries and purpose-built Conference Centre at
        the Science and Industry Museum provide a unique backdrop for corporate meetings and events.</h2>
      <p id="paragraphe" class="col">In a superb city centre location with nearby parking and good road and rail links,
        the Science and Industry Museum is just minutes from Manchester Airport. Our purpose-built Conference Centre is
        located on the second floor of a Grade II listed building in the New Warehouse. The Centre incorporates a
        variety of spaces, which are ideal for conferences, lectures and corporate training sessions for 1 to 300
        guests.

        It also includes a balcony area suitable for registration, displaying company stands and serving refreshments.

        Housed in an historic building, our Revolution Manchester Gallery provides guests with the opportunity to dine
        among some of the world’s oldest exhibits from the field of science and engineering, including the first stored
        computer.

        Our venue hire team will take care of every detail, from choosing your unique space to catering and
        entertainment.

        For further information, please call us on 0161 537 6904 or complete our enquiry form.

        View images of past events and keep up to date on the latest offers by liking us on Facebook and following us on
        Twitter.

        The Science and Industry Museum is proud to be a part of Lime Venue Portfolio.</p>
      <div class="col">
        <p id="P2">If you have any questions don't hesitate to contact us
          <hr>
        </p>
        <div class="form">

          <form>

            <div class="input">
              <input type="email" placeholder="Email" required />
              <input type="tel" placeholder="Telephone number" maxlength="11">
            </div>
            <div class="textarea">
              <textarea placeholder="Your message"></textarea>
            </div>
            <input type="submit" value="Send" />
          </form>
        </div>
      </div>
    </div>
    <hr style="width: 95%">
    <div id="section" class="container-fluid "
      style="padding-top:70px;padding-bottom:70px ;margin-top:50px;; height:800px; ">
      <div class="images">
        <div class="pol">
          <a href="page1.html#image-1"><img src="images/1.jpg" id="img1"></a>

          <div class="container" style="height: 300px; ">
            <h5 style="font-weight: bold ">nom de la salle </h5>
            <p style="font-family:'houssem'; font-weight: bold;">Description:</p>
          </div>
        </div>
        <div class="pol">
          <a href="page1.html#image-2"><img src="images/2.jpg" id="img2"></a>
          <div class="container" style="height: 300px; ">
            <h5 style="font-weight: bold ">nom de la salle </h5>
            <p style="font-family:'houssem'; font-weight: bold;">Description:</p>
          </div>
        </div>
        <div class="pol">

          <a href="page1.html#image-7"><img src="images/7.jpg" id="img7"></a>
          <div class="container" style="height: 300px; ">
            <h5 style="font-weight: bold ">nom de la salle </h5>
            <p style="font-family:'houssem'; font-weight: bold;">Description:</p>
          </div>
        </div>
      </div>
    </div>



    <div id="section1" class="container-fluid "
      style="padding-top:70px;padding-bottom:70px ;margin-top:0px;margin-bottom: 0px; ">

      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Bonum incolumis acies: misera caecitas. Idem iste,
        inquam, de voluptate quid sentit? Aufert enim sensus actionemque tollit omnem. Erat enim Polemonis. Potius
        inflammat, ut coercendi magis quam dedocendi esse videantur. Hoc enim constituto in philosophia constituta sunt
        omnia. Duo Reges: constructio interrete. Dicet pro me ipsa virtus nec dubitabit isti vestro beato M. Sed ut iis
        bonis erigimur, quae expectamus, sic laetamur iis, quae recordamur. Et quidem illud ipsum non nimium probo et
        tantum patior, philosophum loqui de cupiditatibus finiendis. At multis malis affectus. </p>

      <p>Non igitur bene. Gloriosa ostentatio in constituendo summo bono. Maximus dolor, inquit, brevis est. Quid nunc
        honeste dicit? Haec quo modo conveniant, non sane intellego. Haec dicuntur inconstantissime. At enim hic etiam
        dolore. Nunc haec primum fortasse audientis servire debemus. Nummus in Croesi divitiis obscuratur, pars est
        tamen divitiarum. Quae iam oratio non a philosopho aliquo, sed a censore opprimenda est. </p>

      <p>Quae autem natura suae primae institutionis oblita est? Vitiosum est enim in dividendo partem in genere
        numerare. Quae in controversiam veniunt, de iis, si placet, disseramus. Nobis aliter videtur, recte secusne,
        postea; Quid, de quo nulla dissensio est? </p>

    </div>
    <div class="container-fluid" id='footer'>
      <div class="logo">
        <a href="https://www.facebook.com" target="_black"><img src=images/facebook-logo.png alt="" width="40px"
            id="logo1"></a>
        <a href="https://www.google.com/gmail" target="_black"><img src=images/gmail.png alt="" width="40px" id="logo2">
        </a>
        <a href="https://www.whatsapp.com/" target="_black"><img src=images/whatsapp.png alt="" width="40px" id="logo3">
        </a>


      </div>

    </div>

    </div>
  </body>

</html>
<script>
  anime.timeline({ loop: true })
    .add({
      targets: '.ml5 .line',
      opacity: [0.5, 1],
      scaleX: [0, 1],
      easing: "easeInOutExpo",
      duration: 700
    }).add({
      targets: '.ml5 .line',
      duration: 600,
      easing: "easeOutExpo",
      translateY: function (e, i, l) {
        var offset = -0.625 + 0.625 * 2 * i;
        return offset + "em";
      }
    }).add({
      targets: '.ml5 .ampersand',
      opacity: [0, 1],
      scaleY: [0.5, 1],
      easing: "easeOutExpo",
      duration: 600,
      offset: '-=600'
    }).add({
      targets: '.ml5 .letters-left',
      opacity: [0, 1],
      translateX: ["0.5em", 0],
      easing: "easeOutExpo",
      duration: 600,
      offset: '-=300'
    }).add({
      targets: '.ml5 .letters-right',
      opacity: [0, 1],
      translateX: ["-0.5em", 0],
      easing: "easeOutExpo",
      duration: 600,
      offset: '-=600'
    }).add({
      targets: '.ml5',
      opacity: 0,
      duration: 1000,
      easing: "easeOutExpo",
      delay: 1000
    });</script>
<script>
  var textWrapper = document.querySelector('.ml12');
  textWrapper.innerHTML = textWrapper.textContent.replace(/([^\x00-\x80]|\w)/g, "<span class='letter'>$&</span>");

  anime.timeline({ loop: true })
    .add({
      targets: '.ml12 .letter',
      translateX: [40, 0],
      translateZ: 0,
      opacity: [0, 1],
      easing: "easeOutExpo",
      duration: 1200,
      delay: function (el, i) {
        return 500 + 30 * i;
      }
    }).add({
      targets: '.ml12 .letter',
      translateX: [0, -30],
      opacity: [1, 0],
      easing: "easeInExpo",
      duration: 1100,
      delay: function (el, i) {
        return 100 + 30 * i;
      }
    });</script>