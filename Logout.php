<?php
// Start the session
session_start();
 if(!isset($_SESSION['isLogged'])){ //if login in session is not set
    header("Location: Login.php");
    exit (0);
}
// remove all session variables
session_unset(); 
// destroy the session 
session_destroy(); 
header("Location: Login.php");
?>